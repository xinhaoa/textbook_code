public class Test{
	public static void main(String args[]){
		CPU cpu1 = new CPU();
                cpu1.setSpeed(2200);
		HardDisk disk1 = new HardDisk();
		disk1.setAmount(200);
		PC pc1 = new PC(cpu1,disk1);
		CPU cpu2 = new CPU();
                cpu2.setSpeed(2300);
                HardDisk disk2 = new HardDisk();
                disk2.setAmount(200);
		PC pc2 = new PC(cpu2,disk2);
		pc1.show();
		System.out.println("pc1.equals(pc2):"+pc1.equals(pc2));
                System.out.println("cpu1.equals(cpu2):"+cpu1.equals(cpu2));
                System.out.println("disk1.equals(disk2):"+disk1.equals(disk2));
                System.out.println(pc1.toString());
                System.out.println(cpu1.toString());
                System.out.println(disk1.toString());
	
	}
}

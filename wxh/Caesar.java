/**
 * @author wxh
 * @date 2019/5/2
 */


public class Caesar {
    public static void main(String args[]) throws Exception{
        String s=args[0];
        int key=Integer.parseInt(args[1]);
        Movement m=new Movement();
        int n=s.length();
        String es="";
        for(int i=0;i<s.length();i++){
            char c=s.charAt(i);
            if(c >= 'a' && c <= 'z'){
                es=m.realizeMove(n,c,key,'a','z');
            }
            else if (c >= 'A' && c <= 'Z'){
                es=m.realizeMove(n,c,key,'A','Z');
            }
        }
        System.out.println(es);
    }
}

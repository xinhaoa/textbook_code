public class Example7_7 {
   public static void main(String args[]) {
      CargoBoat ship = new CargoBoat();
      ship.setMaxContent(1000);
      ship.setMinContent(105);
      int m =104;
      try{  
           ship.loading(m);
      }
      catch(DangerException2 e) {
           System.out.println(e.warnMess()); 
           System.out.println("不能只装载重量是"+m+"吨的集装箱");       
      }
      finally {
          System.out.printf("货船将正点启航");
      }
  }
}


public class DataStack {//数字栈
	int n;
	Rational [] a;
	int top = -1;
	public DataStack(int m) {
		this.n=m;
	}
	void getStack() {
		a = new Rational[n];
		for(int i=0;i<n;i++) {
			a[i] = new Rational();
		}
	}//确定申请多少空间充当栈
	void push(Rational data) {
		top++;
		a[top].numerator=data.numerator;
		a[top].denominator=data.denominator;
	}//压栈
	Rational pop() {
		return a[top--];
	}//弹栈
}


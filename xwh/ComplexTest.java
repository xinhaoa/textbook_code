import junit.framework.TestCase;

/**
 * @author XWH20175126
 * @date 2019/5/2 21:28
 */
public class ComplexTest extends TestCase {
    Complex com1 = new Complex(1.0, 2.0);
    Complex com2 = new Complex(0.0, 1.0);
    Complex com3 = new Complex(1.0, 0.0);
    Complex com4 = new Complex(-1.0, -2.0);

    public void testgetRealPart() throws Exception {//测试getRealPart方法
        assertEquals(2.7, Complex.getRealPart(2.7));
        assertEquals(-2.7, Complex.getRealPart(-2.7));
        assertEquals(5.0, Complex.getRealPart(5.0));
        assertEquals(0.0, Complex.getRealPart(0.0));
    }

    public void testgetImagePart() throws Exception {//测试getImagePart方法
        assertEquals(2.7, Complex.getImagePart(2.7));
        assertEquals(-2.7, Complex.getImagePart(-2.7));
        assertEquals(5.0, Complex.getImagePart(5.0));
        assertEquals(0.0, Complex.getImagePart(0.0));
    }

    public void testComplexAdd() throws Exception {//测试ComplexAdd方法
        assertEquals("1.0+3.0i", com1.ComplexAdd(com2).toString());
        assertEquals("2.0+2.0i", com1.ComplexAdd(com3).toString());
        assertEquals("1.0+1.0i", com2.ComplexAdd(com3).toString());
        assertEquals("0.0", com1.ComplexAdd(com4).toString());
    }

    public void testComplexSub() throws Exception {//测试ComplexSub方法
        assertEquals("1.0+1.0i", com1.ComplexSub(com2).toString());
        assertEquals("0.0+2.0i", com1.ComplexSub(com3).toString());
        assertEquals("-1.0+1.0i", com2.ComplexSub(com3).toString());
        assertEquals("2.0+4.0i", com1.ComplexSub(com4).toString());
    }

    public void testComplexMulti() throws Exception {//测试ComplexMulti方法
        assertEquals("-2.0+1.0i", com1.ComplexMulti(com2).toString());
        assertEquals("1.0+2.0i", com1.ComplexMulti(com3).toString());
        assertEquals("0.0+1.0i", com2.ComplexMulti(com3).toString());
        assertEquals("3.0 -4.0i", com1.ComplexMulti(com4).toString());
    }

    public void testComplexDiv() throws Exception {//测试ComplexDiv方法
        assertEquals("1.0+2.0i", com1.ComplexDiv(com2).toString());
        assertEquals("2.0+1.0i", com1.ComplexDiv(com3).toString());
        assertEquals("1.0", com2.ComplexDiv(com3).toString());
        assertEquals("-0.8 -1.0i", com1.ComplexDiv(com4).toString());
    }
}

import java.util.Scanner;
public class DIEDAI {
    public static int diedai (int m,int n) {
        if(m==1)
            return n;
        else if(m==0||m==n)
            return 1;
        else if(n<m||n==0)
            return 0;
        else
            return diedai(n-1,m-1)+diedai(n-1,m);
    }
public static void main(String[] args) {
        int [] a = new int [2];
        int sum,i;
        for(i=0;i<=args.length;i++) {
            Scanner sc = new Scanner(System.in);
	    int b = sc.nextInt();
	    a[i] = b;
        }
        sum = diedai(a[0],a[1]);
        if(sum == 0)
            System.out.println("请重新输入");
        else
            System.out.println(sum);
    }
}
